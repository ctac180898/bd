var mongoose = require('mongoose');
var db = mongoose.connection;
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

mongoose.connect('mongodb://stas:adminstas18@ds016108.mlab.com:16108/db-kursach')

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {

});

var record = new mongoose.Schema({
    uid: String,
    firstName: String,
    lastName: String,
    fatherName: String,
    creationDate: Date,
    courtDecisionDate: Date,
    courtDecisionNumber: String,
    orderDate: Date,
    orderNumber: String,
    politicSphere: String,
    punishment: String,
    codeType: String,
    codeArticle: String,
    personId: String,
});

var person = new mongoose.Schema({
    firstName: String,
    lastName: String,
    fatherName: String,
    position: String,
});

var sphere = new mongoose.Schema({
    name: String
});

var user = new mongoose.Schema({
    email: {
        required: true,
        type: String,
        unique: true,
    },
    name: {
        type: String,
        required: true
    },
    hash: String,
    salt: String
})

user.methods.setPassword = function(password){
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
};

user.methods.validPassword = function(password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
};

user.methods.generateJwt = function() {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);

    return jwt.sign({
        _id: this._id,
        email: this.email,
        name: this.name,
        exp: parseInt(expiry.getTime() / 1000),
    }, "MY_SECRET"); // SET SECRET AS ENVIRONMENT VARIABLE IN PRODUCTION APPS
};

mongoose.model('Sphere', sphere);
mongoose.model('User', user);
mongoose.model('Record', record);
mongoose.model('Person', person);

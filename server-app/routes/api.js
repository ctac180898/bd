var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var auth = jwt({
    secret: 'MY_SECRET',
    userProperty: 'payload'
});

var ctrlProfile = require('../controllers/profile');
var ctrlAuth = require('../controllers/authentication');
var recordService = require('../controllers/record');
var sphereService = require('../controllers/sphere');
var personService = require('../controllers/person');

// profile
router.get('/profile', auth, ctrlProfile.profileRead);

// authentication
router.post('/register', ctrlAuth.register);
router.post('/login', ctrlAuth.login);

router.post('/add', recordService.recordAdd);
router.post('/get', recordService.recordsRead);

router.post('/adds', sphereService.sphereAdd);
router.post('/gets', sphereService.spheresRead);

router.post('/addp', personService.personAdd);
router.post('/getp', personService.personsRead);

module.exports = router;

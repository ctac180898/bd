var mongoose = require('mongoose');
var Record = mongoose.model('Record');
var ObjectId = new mongoose.Types.ObjectId;

module.exports.recordsRead = function(req, res) {

    console.log('Records get');
    console.log(req.body);
    Record.find(req.body, function (err, records){
        console.log(records);
        res.status(200);
        res.json(records);
    });
};
module.exports.recordOne = function(req, res) {

    console.log('Records one');
    console.log(req.body);
    //Record.findById( '5b22f60aeb8d460b14e5fce7', function (err, records){
    Record.find( {_id: mongoose.Types.ObjectId('5b22f60aeb8d460b14e5fce7')}, function (err, records){
        console.log(records);
        res.status(200);
        res.json(records);
    });
};
module.exports.recordAdd = function(req, res) {

    console.log('Records add');
    console.log(req.body);
    var record = new Record(req.body);
    record.creationDate = new Date();
    record.save(function(err) {
        res.status(200);
        res.json("Success");
    });
};
module.exports.recordDelete = function(req, res) {

    console.log('Records add');
    console.log(req.body);
    var record = new Record(req.body);
    record.creationDate = new Date();
    record.save(function(err) {
        res.status(200);
        res.json("Success");
    });
};

var mongoose = require('mongoose');
var Person = mongoose.model('Person');
var ObjectId = new mongoose.Types.ObjectId;


module.exports.personsRead = function(req, res) {

    console.log('Persons get');
    console.log(req.body);
    Person.find(req.body, function (err, persons){
        console.log(persons);
        res.status(200);
        res.json(persons);
    });
}
module.exports.personAdd = function(req, res) {

    console.log('Person add');
    console.log(req.body);
    var person = new Person(req.body);
    person.save(function(err) {
        res.status(200);
        res.json("Success");
    });
};

var mongoose = require('mongoose');
var Sphere = mongoose.model('Sphere');
var ObjectId = new mongoose.Types.ObjectId;


module.exports.spheresRead = function(req, res) {

    console.log('Spheres get');
    console.log(req.body);
    Sphere.find(req.body, function (err, spheres){
        console.log(spheres);
        res.status(200);
        res.json(spheres);
    });
}
module.exports.sphereAdd = function(req, res) {

    console.log('Sphere add');
    console.log(req.body);
    var sphere = new Sphere(req.body);
    sphere.save(function(err) {
        res.status(200);
        res.json("Success");
    });
};

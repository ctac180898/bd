import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Record } from '../models/record.model';
import {forEach} from '@angular/router/src/utils/collection';
import {TokenPayload, TokenResponse} from '../models/user.model';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class RecordService {



  url = 'http://localhost:8000';


  constructor(private http: HttpClient, private auth: AuthService) {

  }

  private request(method: 'post', type: 'add' | 'get', record?: Record): Observable<any> {
    let base;

    if (type === 'add') {
      base = this.http.post(this.url + `/api/${type}`, record);
    } else if (type === 'get') {
      base = this.http.post(this.url + `/api/${type}`, record);
    }

    const request = base.pipe(
      map((data: any) => {
        return data;
      })
    );

    return request;
  }

  public add(record: Record): Observable<Record[]> {
    return this.request('post', 'add', record);
  }

  public get(record: Record): Observable<Record[]> {
    return this.request('post', 'get', record);
  }
}

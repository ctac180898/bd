import { Injectable } from '@angular/core';
import {AuthService} from './auth.service';
import {map} from 'rxjs/operators';
import {Record} from '../models/record.model';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Sphere} from '../models/sphere.model';

@Injectable({
  providedIn: 'root'
})
export class SphereService {

  url = 'http://localhost:8000';


  constructor(private http: HttpClient, private auth: AuthService) {

  }

  private request(method: 'post', type: 'adds' | 'gets', sphere?: Sphere): Observable<any> {
    let base;

    if (type === 'adds') {
      base = this.http.post(this.url + `/api/${type}`, sphere);
    } else if (type === 'gets') {
      base = this.http.post(this.url + `/api/${type}`, sphere);
    }

    const request = base.pipe(
      map((data: any) => {
        return data;
      })
    );

    return request;
  }

  public add(sphere: Sphere): Observable<Sphere[]> {
    return this.request('post', 'adds', sphere);
  }

  public get(sphere: Sphere): Observable<Sphere[]> {
    return this.request('post', 'gets', sphere);
  }
}

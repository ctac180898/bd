import { Injectable } from '@angular/core';
import {AuthService} from './auth.service';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Person} from '../models/person.model';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  url = 'http://localhost:8000';


  constructor(private http: HttpClient, private auth: AuthService) {

  }

  private request(method: 'post', type: 'addp' | 'getp', person?: Person): Observable<any> {
    let base;

    if (type === 'addp') {
      base = this.http.post(this.url + `/api/${type}`, person);
    } else if (type === 'getp') {
      base = this.http.post(this.url + `/api/${type}`, person);
    }

    const request = base.pipe(
      map((data: any) => {
        return data;
      })
    );

    return request;
  }

  public add(person: Person): Observable<Person[]> {
    return this.request('post', 'addp', person);
  }

  public get(person: Person): Observable<Person[]> {
    return this.request('post', 'getp', person);
  }
}

export class Person {
  _id: string;
  firstName: string;
  lastName: string;
  fatherName: string;
  position: string;
}

export class Record {
  _id?: string;
  uid: string;
  firstName: string;
  lastName: string;
  fatherName: string;
  creationDate?: Date;
  courtDecisionDate: Date;
  courtDecisionNumber: string;
  orderDate: Date;
  orderNumber: string;
  politicSphere: string;
  punishment: string;
  codeType: string;
  codeArticle: string;
  personId: string;
}

import { Component } from '@angular/core';
import {AuthService} from './services/auth.service';
import {RecordService} from './services/record.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  constructor(private auth: AuthService, private recordService: RecordService){

  }
}

import { Component, OnInit } from '@angular/core';
import {PersonService} from '../../services/person.service';
import {Person} from '../../models/person.model';
import {Record} from '../../models/record.model';
import {RecordService} from '../../services/record.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {

  isPersonsReady = false;
  isRecordsReady = false;
  person: Person;
  persons: Observable<Person[]>;
  pRecords: Observable<Record[]>;

  constructor(private personService: PersonService, private recordService: RecordService) {
    this.person = new Person();
    this.persons = new Observable<Person[]>();
    this.pRecords = new Observable<Record[]>();
  }

  ngOnInit() {
    this.isPersonsReady = false;

  }

  Add() {
    this.personService.add(this.person).subscribe(data => {
      console.log(data);
    });
  }

  Find() {
    this.persons = this.personService.get(this.person);
  }

  getRecords(_id: string) {
    const rec = new Record();
    rec.personId = _id;
    this.pRecords = this.recordService.get(rec);
  }
}

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import {TokenPayload, User} from '../../models/user.model';
import {FormBuilder, Validator, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  credentials: TokenPayload = {
    email: '',
    password: ''
  };

  errorMessage: string;

  constructor(private auth: AuthService, private router: Router) {}

  login() {
    this.auth.login(this.credentials).subscribe(() => {
      this.router.navigateByUrl('/profile');
    }, (err) => {
      this.errorMessage = 'Неправильний мейл чи пароль';
    });
  }


}

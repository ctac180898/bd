import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user;
  isDataReady = false;
  constructor(private auth: AuthService) {
  }

  ngOnInit() {
    this.auth.profile().subscribe(user => {
      this.isDataReady = true;
      this.user = user;
    }, (err) => {
      console.error(err);
    });
  }

}

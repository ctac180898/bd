import { Component, OnInit } from '@angular/core';
import {RecordService} from '../../services/record.service';
import {Record} from '../../models/record.model';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {Sphere} from '../../models/sphere.model';
import {SphereService} from '../../services/sphere.service';
import {PersonService} from '../../services/person.service';
import {Person} from '../../models/person.model';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {


  record: Record;
  spheres: Observable<Sphere[]>;
  persons: Observable<Person[]>;

  constructor( private recordService: RecordService,
               private auth: AuthService,
               private sphereService: SphereService,
               private personService: PersonService) {
    this.record = new Record();
  }

  ngOnInit() {
    this.spheres = this.sphereService.get(new Sphere());
    this.persons = this.personService.get(new Person());
  }

  Add() {
    this.record.uid = this.auth.getUserDetails()._id;
    this.persons.subscribe(persons => {
      const person = persons.filter(p => p._id === this.record.personId)[0];
      this.record.firstName = person.firstName;
      this.record.lastName = person.lastName;
      this.record.fatherName = person.fatherName;
      console.log(this.record);
      this.recordService.add(this.record).subscribe(data => {
        console.log(data);
      });
    });
  }

}

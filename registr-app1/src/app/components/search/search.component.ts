import { Component, OnInit } from '@angular/core';
import {Record} from '../../models/record.model';
import {RecordService} from '../../services/record.service';
import {AuthService} from '../../services/auth.service';
import {SphereService} from '../../services/sphere.service';
import {Sphere} from '../../models/sphere.model';
import {Observable, of} from 'rxjs';
import {PersonService} from '../../services/person.service';
import {Person} from '../../models/person.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  record: Record;
  isDataReady = false;
  spheres: Sphere[];
  sphTmp: Sphere;
  records: Observable<Record[]>;

  constructor( private recordService: RecordService,
               private sphereService: SphereService,
               private auth: AuthService,
               private personService: PersonService ) {
    this.record = new Record();
  }

  ngOnInit() {
    this.sphereService.get(this.sphTmp).subscribe(spheres => {
      this.spheres = spheres;
      this.isDataReady = true;
    });
  }

  Search() {
    this.records = this.recordService.get(this.record);
  }

  Own() {
    this.auth.profile().subscribe(user => {
      const rec = new Record();
      rec.uid = user._id;
      this.records = this.recordService.get(rec);
    });
  }
}

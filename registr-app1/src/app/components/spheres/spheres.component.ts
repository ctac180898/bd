import { Component, OnInit } from '@angular/core';
import {RecordService} from '../../services/record.service';
import {Sphere} from '../../models/sphere.model';
import {SphereService} from '../../services/sphere.service';

@Component({
  selector: 'app-spheres',
  templateUrl: './spheres.component.html',
  styleUrls: ['./spheres.component.scss']
})
export class SpheresComponent implements OnInit {

  sphere: Sphere;
  isSpheresReady = false;
  spheres;

  constructor(private sphereService: SphereService) {
    this.sphere = new Sphere();
  }

  ngOnInit() {
    this.sphereService.get(this.sphere).subscribe(spheres => {
      this.spheres = spheres;
      this.isSpheresReady = true;
    });
  }

  Add() {
    this.sphereService.add(this.sphere).subscribe(data => {
      console.log(data);
    });
  }

}

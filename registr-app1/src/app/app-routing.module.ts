import { Routes } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {SearchComponent} from './components/search/search.component';
import {CreateComponent} from './components/create/create.component';
import {RegisterComponent} from './components/register/register.component';
import {ProfileComponent} from './components/profile/profile.component';
import {AuthGuardService} from './services/auth-guard.service';
import {SpheresComponent} from './components/spheres/spheres.component';
import {PersonComponent} from './components/person/person.component';


export const appRoutes: Routes = [
  { path: '', component: SearchComponent },
  { path: 'sphere', component: SpheresComponent,  canActivate: [AuthGuardService]},
  { path: 'person', component: PersonComponent,  canActivate: [AuthGuardService]},
  { path: 'login', component: LoginComponent },
  { path: 'create', component: CreateComponent, canActivate: [AuthGuardService]},
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent,  canActivate: [AuthGuardService]}
];
